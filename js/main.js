$('.carousel-fade').carousel({
  interval: 8000
});

$('.restricted .carousel').carousel({
  interval: false
});

//Efeitos
var lastScrollTop = 0;
$(window).scroll(function (event) {
    var st = $(this).scrollTop();
    if (st > 200) {                
        $(".navbar-default").addClass('nav-down');
    } else {
        $(".navbar-default").removeClass('nav-down');
    }
    lastScrollTop = st;
});

//Altura Responsiva
var altura = $('.partnersCash > li').width();
$('.partnersCash > li').css('height',altura);

var altura = $('.providers > li').width();
$('.providers > li').css('height',altura);

//Área Restrita
$( document ).ready(function() {
   $( ".restrictedLink" ).click(function() {
    $(this).parent().find('.restricted').toggle();
    return false;
   });
   
});

//Ícones no Accordion
jQuery(function ($) {
    $('[data-toggle="collapse"]').on('click', function() {
        var $this = $(this),
        $parent = typeof $this.data('parent')!== 'undefined' ? $($this.data('parent')) : undefined;
        if($parent === undefined) { /* Just toggle my  */
            $this.find('.fa').toggleClass('fa-plus fa-minus');
            return true;
        }
        /* Open element will be close if parent !== undefined */
        var currentIcon = $this.find('.fa');
        currentIcon.toggleClass('fa-plus fa-minus');
        $parent.find('.fa').not(currentIcon).removeClass('fa-minus').addClass('fa-plus');
    });
});

//Scroll
$('body').scrollspy({ target: '#mainNav' })

//Scroll Suave
$(document).ready(function () {
     $(".nav li a[href^='#']").on('click', function (e) {
         e.preventDefault();

         var target = this.hash,
             $target = $(target);

         $('html, body').stop().animate({
             'scrollTop': $target.offset().top - 200
         }, 900, 'swing', function () {
             window.location.hash = target;             
    
         });
     });
 });
